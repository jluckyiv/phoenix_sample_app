defmodule SampleApp.Acceptance.UserPagesTest do
  use ExUnit.Case
  use Hound.Helpers
  hound_session

  @moduletag :acceptance

  @valid_user %{
    name: "Valid User",
    username: "valid_username",
    email: "valid@email.com",
    password: "1GoodPassword!",
    password_confirmation: "1GoodPassword!"
  }

  setup do
    changeset = SampleApp.User.registration_changeset(%SampleApp.User{}, @valid_user)
    user = SampleApp.Repo.insert!(changeset)

    on_exit fn ->
      SampleApp.Repo.delete!(user)
    end
  end

  test "sign up page has correct title" do
    navigate_to "/users/new"
    assert page_title == "Sign up | Ruby on Rails Tutorial Sample App"
  end

  test "sign up button works" do
    navigate_to "/"
    click({:partial_link_text, "Sign up"})
    assert current_path == "/users/new"
  end

  test "bad user info" do
    navigate_to "/users/new"

    click({:css, ".submit"})
    assert visible_page_text =~ "error"
    assert visible_page_text =~ "can't be blank"

    name = find_element(:css, "#name")
    fill_field(name, "José Valim")
    click({:css, ".submit"})
    assert visible_page_text =~ "error"
    assert visible_page_text =~ "can't be blank"

    name = find_element(:css, "#name")
    username = find_element(:css, "#username")
    fill_field(name, "José Valim")
    fill_field(username, "josevalim")
    click({:css, ".submit"})
    assert visible_page_text =~ "error"
    assert visible_page_text =~ "can't be blank"

    name = find_element(:css, "#name")
    username = find_element(:css, "#username")
    email = find_element(:css, "#email")
    fill_field(name, "José Valim")
    fill_field(username, "josevalim")
    fill_field(email, "jose@valim.com")
    click({:css, ".submit"})
    assert visible_page_text =~ "error"
    assert visible_page_text =~ "can't be blank"

    name = find_element(:css, "#name")
    username = find_element(:css, "#username")
    email = find_element(:css, "#email")
    password = find_element(:css, "#password")
    fill_field(name, "José Valim")
    fill_field(username, "josevalim")
    fill_field(email, "jose@valim.com")
    fill_field(password, "1VeryGoodPassword!")
    click({:css, ".submit"})
    assert visible_page_text =~ "error"
    assert visible_page_text =~ "does not match password"
  end

  test "list and create users" do
    navigate_to "/"
    click({:partial_link_text, "Sign up"})

    fill_field(find_element(:css, "#name"), "José Valim")
    fill_field(find_element(:css, "#username"), "josevalim")
    fill_field(find_element(:css, "#email"), "jose@valim.com")
    fill_field(find_element(:css, "#password"), "1VeryGoodPassword!")
    fill_field(find_element(:css, "#password-confirmation"), "1VeryGoodPassword!")
    click({:css, ".submit"})

    assert current_path == "/users"
    assert find_all_elements(:css, ".user") |> Enum.count == 2
    assert visible_page_text =~ "José Valim created!"

    click({:partial_link_text, "View"})
    assert current_path =~ ~r"/users/\d+"
    assert page_title == "User detail | Ruby on Rails Tutorial Sample App"
  end

  test "show and edit users" do
    navigate_to "/login"

    fill_field(find_element(:css, "#username_or_email"), @valid_user.username)
    fill_field(find_element(:css, "#password"), @valid_user.password)
    click({:css, ".submit"})
    assert current_path == "/users"

    click({:partial_link_text, "View"})
    assert current_path =~ ~r"/users/\d+"
    assert page_title == "User detail | Ruby on Rails Tutorial Sample App"
    # assert visible_page_text =~ "Valid"

    click({:partial_link_text, "Edit"})
    assert current_path =~ ~r"/users/\d+/edit"

    fill_field(find_element(:css, "#name"), "New Name")
    click({:css, ".submit"})
    assert visible_page_text =~ "New"
  end
end
