defmodule SampleApp.Acceptance.AuthTest do
  use ExUnit.Case
  use Hound.Helpers
  hound_session

  @moduletag :acceptance

  @valid_user %{
    name: "Valid User",
    username: "valid_username",
    email: "valid@email.com",
    password: "1GoodPassword!",
    password_confirmation: "1GoodPassword!"
  }

  setup do
    changeset = SampleApp.User.registration_changeset(%SampleApp.User{}, @valid_user)
    user = SampleApp.Repo.insert!(changeset)

    on_exit fn ->
      SampleApp.Repo.delete!(user)
    end
  end

  test "visiting /users requires authentication" do
    navigate_to "/users"
    assert current_path == "/login"
    assert visible_page_text =~ "You must be logged in"
  end

  test "login page has correct title" do
    navigate_to "/login"
    assert page_title == "Login | Ruby on Rails Tutorial Sample App"
  end

  test "login link works" do
    navigate_to "/"
    click({:partial_link_text, "Log in"})
    assert current_path == "/login"
  end

  test "valid login and logout with username" do
    navigate_to "/login"
    fill_field(find_element(:css, "#username_or_email"), @valid_user.username)
    fill_field(find_element(:css, "#password"),@valid_user.password)
    click({:css, ".submit"})

    assert current_path == "/users"
    click({:partial_link_text, "Log out"})
    assert current_path == "/"
  end

  test "valid login and logout with email" do
    navigate_to "/login"
    fill_field(find_element(:css, "#username_or_email"), @valid_user.email)
    fill_field(find_element(:css, "#password"),@valid_user.password)
    click({:css, ".submit"})

    assert current_path == "/users"
    click({:partial_link_text, "Log out"})
    assert current_path == "/"
  end

  test "blank login" do
    navigate_to "/login"
    click({:css, ".submit"})
    assert visible_page_text =~ "Invalid credentials"
    assert current_path == "/sessions"
  end

  test "blank password" do
    navigate_to "/login"
    fill_field(find_element(:css, "#username_or_email"), @valid_user.email)
    click({:css, ".submit"})
    assert visible_page_text =~ "Invalid credentials"
    assert current_path == "/sessions"
  end

  test "bad password" do
    navigate_to "/login"
    fill_field(find_element(:css, "#username_or_email"), @valid_user.email)
    fill_field(find_element(:css, "#password"), "invalid_password")
    click({:css, ".submit"})
    assert visible_page_text =~ "Invalid credentials"
    assert current_path == "/sessions"
  end

  test "nonexistent user" do
    navigate_to "/login"
    fill_field(find_element(:css, "#username_or_email"), "nonexistent_user")
    fill_field(find_element(:css, "#password"), "nonexistent_password")
    click({:css, ".submit"})
    assert visible_page_text =~ "Invalid credentials"
    assert current_path == "/sessions"
  end

  test "creating a new user authenticates that user" do
    navigate_to "/users/new"

    fill_field(find_element(:css, "#name"), "Different User")
    fill_field(find_element(:css, "#username"), "different_user")
    fill_field(find_element(:css, "#email"), "different@user.com")
    fill_field(find_element(:css, "#password"), "1GoodPassword!")
    fill_field(find_element(:css, "#password-confirmation"), "1GoodPassword!")
    click({:css, ".submit"})

    assert current_path == "/users"
  end

end
