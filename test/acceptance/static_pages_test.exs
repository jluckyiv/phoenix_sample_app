defmodule SampleApp.Acceptance.StaticPagesTest do
  use ExUnit.Case
  use Hound.Helpers
  hound_session

  @moduletag :acceptance

  test "home page has correct title" do
    navigate_to "/"
    assert page_title == "Home | Ruby on Rails Tutorial Sample App"
  end

  test "about page has correct title" do
    navigate_to "/about"
    assert page_title == "About | Ruby on Rails Tutorial Sample App"
  end

  test "contact page has correct title" do
    navigate_to "/contact"
    assert page_title == "Contact | Ruby on Rails Tutorial Sample App"
  end

  test "help page has correct title" do
    navigate_to "/help"
    assert page_title == "Help | Ruby on Rails Tutorial Sample App"
  end

  test "home navigation link" do
    navigate_to "/"
    click({:link_text, "Home"})
    assert current_path == "/"
  end

  test "about navigation link" do
    navigate_to "/"
    click({:link_text, "About"})
    assert current_path == "/about"
  end

  test "contact navigation link" do
    navigate_to "/"
    click({:link_text, "Contact"})
    assert current_path == "/contact"
  end

  test "help navigation link" do
    navigate_to "/"
    click({:link_text, "Help"})
    assert current_path == "/help"
  end
end
