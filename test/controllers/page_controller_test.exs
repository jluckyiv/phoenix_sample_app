defmodule SampleApp.PageControllerTest do
  use SampleApp.ConnCase

  test "should get home", %{conn: conn} do
    conn = get conn, "/"
    assert conn.status == 200
    assert html_response(conn, 200) =~ "Home | Ruby on Rails Tutorial Sample App"
  end

  test "should get help", %{conn: conn} do
    conn = get conn, "/help"
    assert conn.status == 200
    assert html_response(conn, 200) =~ "Help | Ruby on Rails Tutorial Sample App"
  end

  test "should get about", %{conn: conn} do
    conn = get conn, "/about"
    assert conn.status == 200
    assert html_response(conn, 200) =~ "About | Ruby on Rails Tutorial Sample App"
  end

  test "should get contact", %{conn: conn} do
    conn = get conn, "/contact"
    assert conn.status == 200
    assert html_response(conn, 200) =~ "Contact | Ruby on Rails Tutorial Sample App"
  end
end
