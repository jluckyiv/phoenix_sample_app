defmodule SampleApp.UserControllerTest do
  use SampleApp.ConnCase

  test "should get new user page", %{conn: conn} do
    conn = get conn, "/users/new"
    assert conn.status == 200
    assert html_response(conn, 200) =~ "Sign up | Ruby on Rails Tutorial Sample App"
  end

end

