defmodule SampleApp.UserTest do
  use SampleApp.ModelCase

  alias SampleApp.User
  import Ecto.Changeset, only: [get_field: 2]

  @valid_attrs %{
    email: "valid@valid.com", name: "Valid Name", username: "valid_username",
    password: "1ValidPassword!", password_confirmation: "1ValidPassword!"
  }

  test "changeset with valid attributes" do
    changeset = User.registration_changeset(%User{}, @valid_attrs)
    assert changeset.valid?
    assert get_field(changeset, :password_hash) != get_field(changeset, :password)
  end

  test "changeset with invalid email" do
    invalid_attrs = Map.put(@valid_attrs, :email, "email")
    changeset = User.registration_changeset(%User{}, invalid_attrs)
    refute changeset.valid?
    assert changeset.errors[:email] == "must be a valid email address"
  end

  test "changeset with invalid name" do
    invalid_attrs = Map.put(@valid_attrs, :name, "     ")
    changeset = User.registration_changeset(%User{}, invalid_attrs)
    refute changeset.valid?
    assert changeset.errors[:name] == "must start with a letter or underscore"
  end

  test "changeset with invalid username" do
    invalid_attrs = Map.put(@valid_attrs, :username, "     ")
    changeset = User.registration_changeset(%User{}, invalid_attrs)
    refute changeset.valid?
    assert changeset.errors[:username] == "must be letters and underscores"
  end

  test "changeset with mismatched passwords" do
    invalid_attrs = Map.put(@valid_attrs, :password_confirmation, "mismatched1Password!")
    changeset = User.registration_changeset(%User{}, invalid_attrs)
    refute changeset.valid?
    assert changeset.errors[:password_confirmation] == "does not match password"
  end

  test "changeset with short password" do
    invalid_password = Map.put(@valid_attrs, :password, "!two3")
    invalid_attrs = Map.put(invalid_password, :password_confirmation, "!two3")
    changeset = User.registration_changeset(%User{}, invalid_attrs)
    refute changeset.valid?
    assert changeset.errors[:password] == {"should be at least %{count} character(s)", [count: 6]}
  end

  test "changeset with long password" do
    invalid_password = Map.put(
      @valid_attrs,
      :password, "!two3-5-10-andtwenty-andthirty-and-forty-and-fifty-and-sixty-&-seventy-an-eighty-an-ninety-1-hundred1"
    )
    invalid_attrs = Map.put(
      invalid_password,
      :password_confirmation, "!two3-5-10-andtwenty-andthirty-and-forty-and-fifty-and-sixty-&-seventy-an-eighty-an-ninety-1-hundred1"
    )
    changeset = User.registration_changeset(%User{}, invalid_attrs)
    refute changeset.valid?
    assert changeset.errors[:password] == {"should be at most %{count} character(s)", [count: 100]}
  end

  test "changeset with blank password" do
    invalid_password = Map.put(@valid_attrs, :password, "          ")
    invalid_attrs = Map.put(invalid_password, :password_confirmation, "          ")
    changeset = User.registration_changeset(%User{}, invalid_attrs)
    refute changeset.valid?
    assert changeset.errors[:password] == "must contain a letter, a number, and a special character"
  end

  test "changeset with number password" do
    invalid_password = Map.put(@valid_attrs, :password, "123456789")
    invalid_attrs = Map.put(invalid_password, :password_confirmation, "123456789")
    changeset = User.registration_changeset(%User{}, invalid_attrs)
    refute changeset.valid?
    assert changeset.errors[:password] == "must contain a letter, a number, and a special character"
  end

  test "changeset with letter password" do
    invalid_password = Map.put(@valid_attrs, :password, "abcdefghij")
    invalid_attrs = Map.put(invalid_password, :password_confirmation, "abcdefghij")
    changeset = User.registration_changeset(%User{}, invalid_attrs)
    refute changeset.valid?
    assert changeset.errors[:password] == "must contain a letter, a number, and a special character"
  end

  test "duplicate email addresses" do
    attrs_1 = %{
      email: "same@email.com", name: "Thing 1", username: "thing1",
      password: "Val!dPa55word", password_confirmation: "Val!dPa55word"
    }
    attrs_2 = %{
      email: "same@email.com", name: "Thing 2", username: "thing2",
      password: "Val!dPa55word", password_confirmation: "Val!dPa55word"
    }
    %User{}
    |> User.registration_changeset(attrs_1)
    |> SampleApp.Repo.insert!
    user2 = %User{}
            |> User.registration_changeset(attrs_2)
    assert {:error, changeset} = Repo.insert(user2)
    assert changeset.errors[:email] == "has already been taken"
  end

  test "duplicate usernames" do
    attrs_1 = %{
      email: "thing1@email.com", name: "Thing 1", username: "same_thing",
      password: "Val!dPa55word", password_confirmation: "Val!dPa55word"
    }
    attrs_2 = %{
      email: "thing2@email.com", name: "Thing 2", username: "same_thing",
      password: "Val!dPa55word", password_confirmation: "Val!dPa55word"
    }
    %User{}
    |> User.registration_changeset(attrs_1)
    |> SampleApp.Repo.insert!
    user2 = %User{}
            |> User.registration_changeset(attrs_2)
    assert {:error, changeset} = Repo.insert(user2)
    assert changeset.errors[:username] == "has already been taken"
  end

  test "mixed-case email" do
    attrs = Map.put(@valid_attrs, :email, "MiXeD_CaSe@eMaIl.CoM")
    changeset = User.registration_changeset(%User{}, attrs)
    assert get_field(changeset, :email) == "mixed_case@email.com"
  end

  test "mixed-case username" do
    attrs = Map.put(@valid_attrs, :username, "My_UserName")
    changeset = User.registration_changeset(%User{}, attrs)
    assert get_field(changeset, :username) == "my_username"
  end
end
