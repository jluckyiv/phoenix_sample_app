defmodule SampleApp.Auth do
  import Plug.Conn
  import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]
  alias SampleApp.User

  def init(opts) do
    Keyword.fetch!(opts, :repo)
  end

  def call(conn, repo) do
    user_id = get_session(conn, :user_id)
    user = user_id && repo.get(User, user_id)
    assign(conn, :current_user, user)
  end

  def login(conn, user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  def login_with_credentials(conn, username_or_email, given_pass, opts) do
    repo = Keyword.fetch!(opts, :repo)
    user = repo.get_by(SampleApp.User, username: username_or_email) || repo.get_by(SampleApp.User, email: username_or_email)

    cond do 
      user && checkpw(to_string(given_pass), to_string(user.password_hash)) ->
        {:ok, login(conn, user)}
      user ->
        {:error, :unauthorized, conn}
      true ->
        dummy_checkpw()
        {:error, :not_found, conn}
    end
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end
end
