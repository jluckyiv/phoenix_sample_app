defmodule SampleApp.SessionController do
  use SampleApp.Web, :controller

  def new(conn, _params) do
    conn
    |> assign(:title, "Login")
    |> render("new.html")
  end

  def create(conn, %{"session" => %{"username_or_email" => user, "password" => pass}}) do
    case SampleApp.Auth.login_with_credentials(conn, user, pass, repo: Repo) do
      {:ok, conn} ->
        conn
        |> put_flash(:info, "Welcome back!")
        |> redirect(to: user_path(conn, :index))
      {:error, _reason, conn} ->
        conn
        |> put_flash(:error, "Invalid credentials.")
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> SampleApp.Auth.logout()
    |> redirect(to: page_path(conn, :home))
  end
end
