defmodule SampleApp.UserController do
  use SampleApp.Web, :controller
  alias SampleApp.Repo
  alias SampleApp.User

  plug :scrub_params, "user" when action in [:create]
  plug :authenticate when action in [:index, :show]

  def index(conn, _params) do
    users = Repo.all(User)
    conn
    |> assign(:title, "Users")
    |> render("index.html", users: users)
  end

  def show(conn, %{"id" => id}) do
    user = Repo.get(User, id)
    conn
    |> assign(:title, "User detail")
    |> render("show.html", user: user)
  end

  def new(conn, _params) do
    changeset = User.changeset(%User{})
    conn
    |> assign(:title, "Sign up")
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.registration_changeset(%User{}, user_params)
    case Repo.insert(changeset) do
      {:ok, user} ->
        conn
        |> SampleApp.Auth.login(user)
        |> put_flash(:info, "#{user.name} created!")
        |> redirect(to: user_path(conn, :index))
      {:error, changeset} ->
        conn
        |> render("new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    user = Repo.get(User, id)
    changeset = User.changeset(user)
    conn
    |> assign(:title, "Edit")
    |> render("edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Repo.get(User, id)
    changeset = User.changeset(user, user_params)
    case Repo.update(changeset) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "#{user.name} updated!")
        |> redirect(to: user_path(conn, :show, user))
      {:error, changeset} ->
        conn
        |> render("edit.html", user: user, changeset: changeset)
    end
  end

  defp authenticate(conn, _opts) do
    if conn.assigns.current_user do
      conn
    else
      conn
      |> put_flash(:error, "You must be logged in to access that page")
      |> redirect(to: session_path(conn, :new))
      |> halt()
    end
  end
end
