defmodule SampleApp.User do
  use SampleApp.Web, :model

  schema "users" do
    field :name, :string
    field :email, :string
    field :username, :string
    field :password_hash, :string

    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    timestamps
  end

  @required_fields ~w(name email username password password_confirmation)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def registration_changeset(model, params \\ :empty) do
    model
    |> changeset(params)
    |> cast(params, ~w(password password_confirmation), [])
    |> validate_password
    |> put_hash_password
  end

  def changeset(model, params \\ :empty) do
    model
    |> cast(params, ~w(email name username), [])
    |> put_downcase(:email)
    |> validate_email
    |> validate_name
    |> put_downcase(:username)
    |> validate_username
  end

  defp validate_email(changeset) do
    changeset
    |> validate_length(:email, min: 6, max: 255)
    |> validate_format(:email, ~r/@/, message: "must be a valid email address")
    |> unique_constraint(:email)
  end

  defp validate_name(changeset) do
    changeset
    |> validate_length(:name, min: 1, max: 50)
    |> validate_format(:name, ~r/^\w.+/, message: "must start with a letter or underscore")
  end

  defp validate_username(changeset) do
    changeset
    |> validate_length(:username, min: 1, max: 20)
    |> validate_format(:username, ~r/^\w+$/, message: "must be letters and underscores")
    |> unique_constraint(:username)
  end

  defp validate_password(changeset) do
    changeset
    |> validate_length(:password, min: 6, max: 100)
    |> validate_format(
      :password,
      ~r"(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]",
      message: "must contain a letter, a number, and a special character"
    )
    |> validate_confirmation(:password, message: "does not match password")
  end

  defp put_hash_password(%{valid?: false} = changeset), do: changeset
  defp put_hash_password(%{valid?: true, changes: %{password: plainpw}} = changeset) do
    changeset
    |> put_change(:password_hash, Comeonin.Bcrypt.hashpwsalt(plainpw))
  end

  defp put_downcase(changeset, field) do
    changeset
      |> update_change(field, &String.downcase/1)
  end

end
