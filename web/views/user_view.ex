defmodule SampleApp.UserView do
  use SampleApp.Web, :view
  alias SampleApp.User

  def first_name(%User{name: name}) do
    name
    |> String.split(" ")
    |> Enum.at(0)
  end
end

